const express = require('express');
const router = express.Router();

const cmoEvent = require('../models/eventsSchema')

const { authenticateToken, authorizeRoles } = require('../middleware');

// EVENTS TABLE CRUD

    // POST EVENT
    router.post('/event', authenticateToken, authorizeRoles('Admin', 'Staff'), async (req,res) => {
        try {
            const { 
                eventDate, 
                department,
                description, 
                batch,
                yearLevel, 
                venue,
                inCharge } = req.body

            const parsedEventDate = new Date(eventDate);
            
            const newEvent = new cmoEvent({
                eventDate: parsedEventDate, 
                department,
                description, 
                batch,
                yearLevel, 
                venue,
                inCharge
            })
            await newEvent.save()
            res.status(201).json({message : 'Event added successfully'})
        } catch (error) {
            res.status(500).json({error: 'Error adding event'})
        }
    })


// GET EVENTS - Table Search, Date Range, and Pagination
router.get('/events-list', async (req, res) => {
    try {
        // Parsing page and limit from the query parameters
        const page = parseInt(req.query.page) || 1;
        const limit = parseInt(req.query.limit) || 10;
        const { startDate, endDate, keyword } = req.query;

        // Building the search query based on the keyword
        let query = {};
        if (keyword) {
            query.$or = [
                { department: { $regex: new RegExp(keyword, 'i') } },
                { description: { $regex: new RegExp(keyword, 'i') } },
                { batch: { $regex: new RegExp(keyword, 'i') } },
                { yearLevel: { $regex: new RegExp(keyword, 'i') } },
                { venue: { $regex: new RegExp(keyword, 'i') } },
                { inCharge: { $regex: new RegExp(keyword, 'i') } }
            ];
        }

        if (startDate && endDate) {
            query.eventDate = { $gte: new Date(startDate), $lte: new Date(endDate) };
        }

        const skip = (page - 1) * limit;

        // Retrieving events and the total event count
        const events = await cmoEvent.find(query).sort({ eventDate: 1 }).skip(skip).limit(limit);
        const totalEvents = await cmoEvent.countDocuments(query);

        // Responding with the events data and pagination details
        res.json({
            data: events,
            currentPage: page,
            totalPages: Math.ceil(totalEvents / limit),
            totalCount: totalEvents
        });
    } catch (error) {
        console.error('Error fetching events:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});


// PUT EVENT INFO
router.put('/edit-event/:id', authenticateToken, authorizeRoles('Admin', 'Staff'), async(req, res) => {
    try {
        const {id} = req.params;
        const event = await cmoEvent.findByIdAndUpdate(id, req.body);

        if(!event){
            return res.status(404).json({message: `Cannot find event with ID: ${id}`})
        }
        const updatedEvent = await cmoEvent.findById(id);
        res.status(200).json({updatedEvent});

    } catch (error) {
        res.status(500).json({error: 'Unable to update event, please try again'})
    }
})


// DELETE EVENT INFO
router.delete('/delete-event/:id', authenticateToken, authorizeRoles('Admin', 'Staff'), async(req, res) => {
    try {
        const {id} = req.params;
        const event = await cmoEvent.findByIdAndDelete(id);

        if(!event){
            return res.status(404).json({message: `Cannot find event with ID: ${id}`})
        }
        res.status(200).json({message: `Successfully deleted Event ID: ${id}`});

    } catch (error) {
        res.status(500).json({error: 'Unable to delete event, please try again'})
    }
})

module.exports = router;