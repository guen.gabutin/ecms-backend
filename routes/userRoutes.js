require('dotenv').config();
const express = require('express');
const router = express.Router();
const bcrypt = require ('bcryptjs')
const crypto = require('crypto');
const jwt = require ('jsonwebtoken')

const SECRET_KEY = process.env.SECRET_KEY;

const User = require('../models/userSchema')
const { authenticateToken, authorizeRoles } = require('../middleware');

const AWS = require('aws-sdk');

AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  });

  const ses = new AWS.SES({
    region: process.env.AWS_REGION
  });


// Helper function to validate email format
const validateEmail = (email) => {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
};

// Routes
// USER REGISTRATION
// POST REGISTER
router.post('/create-account', authenticateToken, authorizeRoles('Admin'), async (req,res) => {
    try {
        const { email, username, password, confirmPassword, role } = req.body;

        // Validate email format
        if (!validateEmail(email)) {
            return res.status(400).json({ message: 'Invalid email format' });
        }

        // Check password confirmation
        if (password !== confirmPassword) {
            return res.status(400).json({ message: 'Passwords do not match' });
        }

        // Continue with the existing logic
        const hashedPassword = await bcrypt.hash(password, 10);
        const newUser = new User({ email, username, password: hashedPassword, role });
        await newUser.save();

        res.status(201).json({ message: 'User created successfully' });
    } catch (error) {

        res.status(500).json({ error: 'Error signing up' });
    }
});

// GET REGISTERED USERS
router.get('/user-list', async (req,res) => {
    try {
        const page = parseInt(req.query.page) || 1;
        const limit = parseInt(req.query.limit) || 5;
        const keyword = req.query.keyword;

        // Building the query condition based on the keyword
        let queryCondition = {};
        if (keyword) {
            const regex = new RegExp(keyword, 'i');  // 'i' for case-insensitive
            queryCondition = {
                $or: [
                    { email: { $regex: regex } },
                    { username: { $regex: regex } },
                    { role: { $regex: regex } }
                ]
            };
        }

        const skip = (page - 1) * limit;

        // Applying the search condition on the find query
        const users = await User.find(queryCondition).skip(skip).limit(limit);
        const totalCount = await User.countDocuments(queryCondition);

        // Explicitly setting Content-Type to 'application/json'
        res.type('application/json');
        res.status(200).json({
            data: users,
            currentPage: page,
            totalPages: Math.ceil(totalCount / limit),
            totalCount: totalCount
        });
    } catch (error) {
        res.type('application/json');
        res.status(500).json({ error: 'Unable to get users' });
    }
});

// EDIT REGISTERED USER
router.put('/edit-user/:id', authenticateToken, authorizeRoles('Admin'), async (req, res) => {
    try {
        const { email, username, password, confirmPassword, role } = req.body;
        if (email && !validateEmail(email)) {
            return res.status(400).json({ message: 'Invalid email format' });
        }

        // if (password && password !== confirmPassword) {
        //     return res.status(400).json({ message: 'Passwords do not match' });
        // }

        const updatedData = { email, username, role };

        if (password) {
            updatedData.password = await bcrypt.hash(password, 10);
        }

        // Remove any undefined fields
        Object.keys(updatedData).forEach(key => updatedData[key] === undefined && delete updatedData[key]);

        const updatedUser = await User.findByIdAndUpdate(req.params.id, updatedData, { new: true });
        if (!updatedUser) {
            return res.status(404).json({ message: 'User not found' });
        }

        res.status(200).json({ message: 'User updated successfully', updatedUser });
    } catch (error) {
        res.status(500).json({ error: 'Error updating user' });
    }
});


// router.put('/edit-user/:id', authenticateToken, authorizeRoles('Admin'), async (req, res) => {
//     try {
//         const { email, username, role } = req.body;
//         if (email && !validateEmail(email)) {
//             return res.status(400).json({ message: 'Invalid email format' });
//         }

//         const updatedData = { email, username, role };

//         // Remove any undefined fields
//         Object.keys(updatedData).forEach(key => updatedData[key] === undefined && delete updatedData[key]);

//         const updatedUser = await User.findByIdAndUpdate(req.params.id, updatedData, { new: true });
//         if (!updatedUser) {
//             return res.status(404).json({ message: 'User not found' });
//         }

//         res.status(200).json({ message: 'User updated successfully', updatedUser });
//     } catch (error) {
//         res.status(500).json({ error: 'Error updating user' });
//     }
// });

// DELETE REGISTERED USER
router.delete('/delete-user/:id', authenticateToken, authorizeRoles('Admin'), async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        res.status(200).json({ message: 'User deleted successfully' });
    } catch (error) {
        res.status(500).json({ error: 'Error deleting user' });
    }
});

router.post('/login', async (req, res) => {
    try {
        const { username, password } = req.body;
        const user = await User.findOne({ username });
        if (!user) {
            return res.status(401).json({ error: 'Invalid credentials' });
        }
        const isPasswordValid = await bcrypt.compare(password, user.password);
        if (!isPasswordValid) {
            return res.status(401).json({ error: 'Invalid password' });
        }

        console.log('Sending username:', user.username);

        const token = jwt.sign({ userID: user._id, role: user.role, username: user.username }, SECRET_KEY, { expiresIn: '1hr' });
        // Include username in the response here
        res.json({ message: 'Login Success', token, role: user.role, username: user.username });  // Now also sending username
    } catch (error) {
        res.status(500).json({ error: 'Login Error' });
    }
});

router.post('/verify-token', (req, res) => {
    const { token } = req.body;
    if (!token) {
        // console.log('No token provided');
        return res.status(400).json({ message: 'No token provided' });
    }

    jwt.verify(token, SECRET_KEY, (err, decoded) => {
        if (err) {
            // console.log(`Token verification failed: ${err.message}`);
            return res.status(401).json({ message: 'Invalid token' });
        }

        // console.log(`Token verified successfully: ${JSON.stringify(decoded)}`);
        // Optionally add more checks or return part of the decoded token
        res.json({ valid: true, role: decoded.role });
    });
});


// Function to send password reset email using Amazon SES
// const sendPasswordResetEmail = (email, resetUrl) => {
//     return new Promise((resolve, reject) => {
//         const params = {
//             Source: process.env.AWS_SOURCE_EMAIL,
//             Destination: {
//                 ToAddresses: [email]
//             },
//             Message: {
//                 Subject: {
//                     Data: 'ECMS Password Reset Request'
//                 },
//                 Body: {
//                     Html: {
//                         Data: `<html><body><p>Please click the link below to reset your password:</p><a href="${resetUrl}">${resetUrl}</a></body></html>`
//                     }
//                 }
//             }
//         };

//         ses.sendEmail(params, (err, data) => {
//             if (err) {
//                 // console.log(err, err.stack); // an error occurred
//                 reject(err);
//             } else {
//                 // console.log(data);           // successful response
//                 resolve(data);
//             }
//         });
//     });
// };

// Forgot password endpoint
// router.post('/forgot-password', async (req, res) => {
//     const { email } = req.body;
//     const user = await User.findOne({ email: email });

//     if (!user) {
//         return res.status(200).json({ message: 'If the email is associated with an account, a reset link has been sent.' });
//     }

//     const resetToken = crypto.randomBytes(20).toString('hex');
//     user.resetPasswordToken = resetToken;
//     user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
//     await user.save();

//     const resetUrl = `http://xuecms.com/reset-password/${resetToken}`;

//     try {
//         await sendPasswordResetEmail(user.email, resetUrl);
//         res.status(200).json({ message: 'Password reset link sent to email.' });
//     } catch (error) {
//         res.status(500).json({ message: 'Error sending email', error });
//     }
// });


// Express route to handle the password reset logic
// router.post('/reset-password/:token', async (req, res) => {
//     const { token } = req.params;
//     const { password } = req.body;

//     const user = await User.findOne({
//         resetPasswordToken: token,
//         resetPasswordExpires: { $gt: Date.now() }
//     });

//     if (!user) {
//         return res.status(400).json({ message: 'Token is invalid or has expired.' });
//     }

//     // Set the new password and clear the reset token and expiration
//     user.password = await bcrypt.hash(password, 10);
//     user.resetPasswordToken = undefined;
//     user.resetPasswordExpires = undefined;

//     await user.save();

//     res.status(200).json({ message: 'Password has been updated.' });
// });


module.exports = router;