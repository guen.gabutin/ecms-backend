const express = require('express');
const router = express.Router();

const AWS = require('aws-sdk');
const multer = require('multer');

const certTemplate = require('../models/certTemplateSchema')

const { authenticateToken, authorizeRoles } = require('../middleware');

// AWS Config to use credentials
AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  });

// Helper function to prepare upload parameters for S3
const fileUploadParams = (file, type) => {
  let keyPath = '';
  const filename = `${file.originalname}`;

  if (type === 'background') {
      keyPath = `Background-images/${filename}`;
  } else if (type === 'signature') {
      keyPath = `Director-signatures/${filename}`;
  }

  return {
      Bucket: process.env.AWS_S3_BUCKET_NAME,
      Key: keyPath,
      Body: file.buffer
  };
};

// Utility function to delete a file from S3
const deleteFileFromS3 = (filePath) => {
    const s3 = new AWS.S3();
    const params = {
      Bucket: process.env.AWS_S3_BUCKET_NAME,
      Key: filePath,
    };
  
    return s3.deleteObject(params).promise();
  };

const storage = multer.memoryStorage(); // Files will be stored in memory temporarily

const fileFilter = (req, file, cb) => {
    // Accept images only
    if (file.mimetype.startsWith('image')) {
        cb(null, true);
    } else {
        cb(new Error('Not an image! Please upload only images.'), false);
    }
};

const uploadImage = multer({ storage: storage, fileFilter: fileFilter });

const upload = multer({ storage: storage, fileFilter: fileFilter }).fields([
    { name: 'certBgImg', maxCount: 1 },
    { name: 'certSigImg', maxCount: 1 }
]);

// GET TEMPLATE
router.get('/templates-list', authenticateToken, authorizeRoles('Admin', 'Staff'), async (req, res) => {
    try {
        const keyword = req.query.keyword;

        let searchQuery = {};
        if (keyword) {
            const regex = new RegExp(keyword, 'i');  // 'i' for case-insensitive

            // Using $or to search across multiple fields with a single keyword
            searchQuery = {
                $or: [
                    { templateTitle: { $regex: regex } },
                    { certBgImgKey: { $regex: regex } },
                    { certEventYearLevel: { $regex: regex } },
                    { certEventType: { $regex: regex } },
                    { certEventTheme: { $regex: regex } },
                    { certEventVenue: { $regex: regex } },
                    { certDirectorName: { $regex: regex } },
                    { certSigImgKey: { $regex: regex } }
                ]
            };
        
            // Optional: Handle date separately if keyword can be a date
            if (keyword.match(/^\d{4}-\d{2}-\d{2}$/)) {
                const dateQuery = new Date(keyword);
                if (!isNaN(dateQuery.valueOf())) { // Check if the keyword is a valid date
                    searchQuery.$or.push({ certEventDate: { $eq: dateQuery } });
                }
            }
        }

        // Find templates based on the constructed search query
        const templates = await certTemplate.find(searchQuery);
        const total = await certTemplate.countDocuments(searchQuery);

        // Return all the templates that match the query
        res.status(200).json({
            data: templates,
            total: total
        });
    } catch (err) {
        console.error("Error retrieving templates:", err);
        res.status(500).send(err.toString());
    }
});

// UPLOAD IMAGE - HELPER ENDPOINT
router.post('/upload-image', authenticateToken, uploadImage.single('image'), async (req, res) => {
    if (!req.file) {
        return res.status(400).send('No image uploaded.');
    }

    const s3 = new AWS.S3();
    const file = req.file;
    const filename = `${Date.now()}_${file.originalname}`; // Rename the file to avoid name conflicts in S3

    const uploadParams = {
        Bucket: process.env.AWS_S3_BUCKET_NAME,
        Key: `uploaded-images/${filename}`, // Adjust the path as needed
        Body: file.buffer,
        // ACL: 'public-read' // or 'private' depending on your requirement
    };

    try {
        // Upload the file to S3
        const uploadResult = await s3.upload(uploadParams).promise();

        // Send back the key of the uploaded file
        res.status(200).json({ key: uploadResult.Location });
    } catch (error) {
        console.error('Error uploading to S3:', error);
        res.status(500).send('Error uploading image');
    }
});

// POST TEMPLATE - Generate Template Endpoint (with Background and Signature Img)
router.post('/generate-template', authenticateToken, authorizeRoles('Admin', 'Staff'), upload, (req, res) => {
    // Log the received files and body to debug potential issues with file uploads or missing fields
    // console.log("Files received:", req.files);
    // console.log("Form data received:", req.body);

    const { templateTitle,
            certEventYearLevel,
            certEventType,
            certEventTheme,
            certEventDate,
            certEventVenue,
            certDirectorName
          } = req.body;

    // Check if the necessary files are available in req.files
    if (!req.files || !req.files.certBgImg || !req.files.certSigImg) {
        return res.status(400).send('Required images are missing. Please ensure that both background and signature images are included.');
    }

    const s3 = new AWS.S3();

    // Process background image upload to S3
    const bgImgParams = fileUploadParams(req.files.certBgImg[0], 'background');
    s3.upload(bgImgParams, (err, data) => {
        if (err) {
            console.error('Error uploading background image:', err);
            return res.status(500).send('Failed to upload background image');
        }

        const bgImgKey = data.Location;

        // Process signature image upload to S3
        const sigImgParams = fileUploadParams(req.files.certSigImg[0], 'signature');
        s3.upload(sigImgParams, (err, data) => {
            if (err) {
                console.error('Error uploading signature image:', err);
                return res.status(500).send('Failed to upload signature image');
            }

            const sigImgKey = data.Location;

            // Save new certificate template to MongoDB
            const newCertTemplate = new certTemplate({
                templateTitle,
                certBgImgKey: bgImgKey,
                certEventYearLevel,
                certEventType,
                certEventTheme,
                certEventDate,
                certEventVenue,
                certDirectorName,
                certSigImgKey: sigImgKey
            });

            newCertTemplate.save()
                .then(doc => {
                    res.status(201).send(doc);
                })
                .catch(err => {
                    console.error('Error saving the certificate template to MongoDB:', err);
                    res.status(500).send('Failed to save the certificate template');
                });
        });
    });
});

  // PUT TEMPLATE - Updates template, including images used
  router.put('/update-template/:id', authenticateToken, authorizeRoles('Admin', 'Staff'), upload, async (req, res) => {
    try {
        const templateId = req.params.id;
        const {
            templateTitle,
            certEventYearLevel,
            certEventType,
            certEventTheme,
            certEventDate,
            certEventVenue,
            certDirectorName
        } = req.body;

        const existingTemplate = await certTemplate.findById(templateId);
    
        if (!existingTemplate) {
          return res.status(404).send('Template not found');
        }

        let bgImgKey = existingTemplate.certBgImgKey;
        let sigImgKey = existingTemplate.certSigImgKey;
        const s3 = new AWS.S3();

        // Handle background image update
        if (req.files.certBgImg && req.files.certBgImg.length > 0) {
            // Assuming deleteFileFromS3 is an existing, implemented function
            await deleteFileFromS3(bgImgKey);
            const bgImgParams = fileUploadParams(req.files.certBgImg[0], 'background');
            const bgUploadResult = await s3.upload(bgImgParams).promise();
            bgImgKey = bgUploadResult.Location;
        }

        // Handle signature image update
        if (req.files.certSigImg && req.files.certSigImg.length > 0) {
            // Assuming deleteFileFromS3 is an existing, implemented function
            await deleteFileFromS3(sigImgKey);
            const sigImgParams = fileUploadParams(req.files.certSigImg[0], 'signature');
            const sigUploadResult = await s3.upload(sigImgParams).promise();
            sigImgKey = sigUploadResult.Location;
        }

        // Update MongoDB document with new or existing data
        const updatedData = {
            templateTitle,
            certBgImgKey: bgImgKey,
            certEventYearLevel,
            certEventType,
            certEventTheme,
            certEventDate,
            certEventVenue,
            certDirectorName,
            certSigImgKey: sigImgKey
        };

        const updatedTemplate = await certTemplate.findByIdAndUpdate(templateId, updatedData, { new: true });
        res.status(200).send(updatedTemplate);
    } catch (err) {
        res.status(500).send(err.toString());
    }
});

  // DELETE TEMPLATE - Deletes template, including images used
  router.delete('/delete-template/:id', authenticateToken, authorizeRoles('Admin', 'Staff'), async (req, res) => {
    try {
        const templateId = req.params.id;

        const templateToDelete = await certTemplate.findById(templateId);
        if (!templateToDelete) {
            return res.status(404).send('Template not found');
        }

        // Check if other templates are using the same background image
        const bgImgKey = templateToDelete.certBgImgKey.split('/').pop();
        const otherBgUsage = await certTemplate.findOne({ certBgImgKey: templateToDelete.certBgImgKey, _id: { $ne: templateId } });

        if (!otherBgUsage && templateToDelete.certBgImgKey) {
            await deleteFileFromS3(`Background-images/${bgImgKey}`);
        }

        // Check if other templates are using the same signature image
        const sigImgKey = templateToDelete.certSigImgKey.split('/').pop();
        const otherSigUsage = await certTemplate.findOne({ certSigImgKey: templateToDelete.certSigImgKey, _id: { $ne: templateId } });

        if (!otherSigUsage && templateToDelete.certSigImgKey) {
            await deleteFileFromS3(`Director-signatures/${sigImgKey}`);
        }

        await certTemplate.findByIdAndDelete(templateId);

        res.status(200).send({ message: 'Template deleted successfully' });
    } catch (err) {
        res.status(500).send(err.toString());
    }
});

module.exports = router;