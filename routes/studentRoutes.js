const express = require('express');
const router = express.Router();

const student = require('../models/studentsSchema')
const generatedCertificates = require('../models/certificatesSchema')

const { authenticateToken, authorizeRoles } = require('../middleware');

// STUDENTS TABLE CRUD

    // POST STUDENT
    router.post('/add-student', authenticateToken, authorizeRoles('Admin', 'Staff'), async (req,res) => {
        try {
            const {
                studentId,
                college,
                department,
                // program,
                major,
                email,
                firstName,
                lastName,
                yearStanding
            } = req.body;
    
            // Check if studentId already exists
            const existingStudent = await student.findOne({ studentId: studentId });
            if (existingStudent) {
                return res.status(400).json({ error: 'Student ID already exists' });
            }
    
            // Create new student with batchIds initialized as an empty array
            const newStudent = new student({
                studentId,
                college,
                department,
                // program,
                major,
                email,
                firstName,
                lastName,
                yearStanding,
                batchIds: [] // Initialize batchIds as an empty array
            });
    
            await newStudent.save();
            res.status(201).json({ message: 'Student added successfully' });
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Error adding student' });
        }
    });


    // GET STUDENTS - College Selection, Table Operations and Pagination
    router.get('/student-list', async (req, res) => {
        try {
            const page = parseInt(req.query.page) || 1;
            const limit = parseInt(req.query.limit) || 10;
            const keyword = req.query.keyword;
            const college = req.query.college;
            const department = req.query.department;
    
            // Building the query condition based on the keyword, college, and department
            let queryCondition = {};
    
            if (college) {
                queryCondition.college = college;
            } else {
                return res.status(400).json({ error: 'College query parameter is required' });
            }
    
            if (department) {
                queryCondition.department = department;
            }
    
            if (keyword) {
                const regex = new RegExp(keyword, 'i'); // 'i' for case-insensitive
                queryCondition.$or = [
                    { studentId: { $regex: regex } },
                    { lastName: { $regex: regex } },
                    { firstName: { $regex: regex } },
                    { department: { $regex: regex } },
                    { yearStanding: { $regex: regex } }
                ];
            }
    
            const skip = (page - 1) * limit;
    
            // Applying the search condition on the find query
            const students = await student.find(queryCondition)
                .skip(skip)
                .limit(limit);
    
            const totalCount = await student.countDocuments(queryCondition);
    
            // Explicitly setting Content-Type to 'application/json'
            res.type('application/json');
            res.status(200).json({
                data: students,
                currentPage: page,
                totalPages: Math.ceil(totalCount / limit),
                totalCount: totalCount
            });
        } catch (error) {
            res.type('application/json');
            res.status(500).json({ error: 'Unable to get students' });
        }
    });

    // GET STUDENT RECORD -  Fetch student information, certificate count, and etc.
    router.get('/student-profile', authenticateToken, authorizeRoles('Admin', 'Staff', 'StudentAssistant'), async (req, res) => {
        const { studentId } = req.query;
    
        // Validate studentId parameter
        if (!studentId) {
            return res.status(400).send('studentId query parameter is required.');
        }
    
        try {
            // Find the student by studentId
            const existingStudent = await student.findOne({ studentId: studentId });
            if (!existingStudent) {
                return res.status(404).send('Student not found.');
            }
    
            // Concatenate student's first name and last name
            const studentName = `${existingStudent.firstName} ${existingStudent.lastName}`;
    
            // Assuming department and yearStanding are arrays, concatenate their elements
            const departmentYearStanding = `${existingStudent.department} - ${existingStudent.yearStanding}`;
    
            // Fetch related generated certificate records for this student
            const certificates = await generatedCertificates.find({ studentInfo: existingStudent._id }).populate('studentInfo').select('certificateId certificateURL createdAt createdBy');
            
            // Format the certificate records for table display
            const formattedCertificates = certificates.map(cert => ({
                certificateId: cert.certificateId,
                certificateURL: cert.certificateURL,
                Name: `${cert.studentInfo.firstName} ${cert.studentInfo.lastName}`,  // Safe to access since it's populated
                DateGenerated: cert.createdAt,
                CreatedBy: cert.createdBy || 'Unknown'
            }));
    
            // Return the response with student information and certificate records
            res.json({
                studentName: studentName,
                studentId: existingStudent.studentId,
                departmentYearStanding: departmentYearStanding,
                certificatesTotal: formattedCertificates.length,
                certificates: formattedCertificates
            });
        } catch (error) {
            console.error('Error fetching student information:', error);
            res.status(500).send('Internal server error');
        }
    });


    // PUT STUDENT INFO
    router.put('/edit-student/:id', authenticateToken, authorizeRoles('Admin', 'Staff'), async(req, res) => {
        try {
            const { id } = req.params;
            const studentInfo = await student.findByIdAndUpdate(id, req.body);

            if (!studentInfo) {
                return res.status(404).json({ message: `Cannot find student with ID: ${id}` });
            }
            const updatedStudentInfo = await student.findById(id);
            res.status(200).json({updatedStudentInfo});

        } catch (error) {
            console.error('Unable to update student info:', error);
            res.status(500).json({ error: 'Unable to update student info, please try again' });
        }
    })


    // DELETE STUDENT INFO
    router.delete('/delete-student/:id', authenticateToken, authorizeRoles('Admin', 'Staff'), async(req, res) => {
        try {
            const { id } = req.params;
            const studentInfo = await student.findByIdAndDelete(id, req.body);

            if(!studentInfo){
                return res.status(404).json({message: `Cannot find student with ID: ${id}`})
            }
            res.status(200).json({message: `Successfully deleted Student ID: ${id}`});

        } catch (error) {
            res.status(500).json({error: 'Unable to delete student info, please try again'})
        }
    })

module.exports = router;