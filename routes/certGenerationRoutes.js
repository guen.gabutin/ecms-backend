require('dotenv').config();

const express = require('express');

// Adjust multer setup to use memory storage
const multer = require('multer');
const memoryStorage = multer.memoryStorage();
const upload = multer({ storage: memoryStorage });

// const multer = require('multer');
// const upload = multer({ dest: 'uploads/' });

const csvParser = require('csv-parser');
const fs = require('fs');

const PDFDocument = require('pdfkit');
const QRCode = require('qrcode');
const AWS = require('aws-sdk');

const { v4: uuidv4 } = require('uuid');
const crypto = require('crypto');

const nodemailer = require('nodemailer');
const sgMail = require('@sendgrid/mail')

const Student = require('../models/studentsSchema');
const certTemplate = require('../models/certTemplateSchema')
const generatedCertificates = require('../models/certificatesSchema')

const { authenticateToken, authorizeRoles } = require('../middleware');

const router = express.Router();

sgMail.setApiKey(process.env.TWILIO_API_KEY)

  // Create a reusable transporter object using the default SMTP transport
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.GMAIL_USER,  // Your Gmail address
      pass: process.env.GMAIL_APP_PASSWORD  // Your Gmail password
    }
  });

// Initialize AWS SDK for S3 and SES
AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});
const s3 = new AWS.S3();


// Function to extract key from the S3 Object URL
function extractKeyFromUrl(url) {
  const urlParts = new URL(url);
  // This will extract the key from the path. It assumes that the bucket name is part of the path and removes it.
  return urlParts.pathname.substring(1); // remove the leading slash
}

// Rollback helper for S3 deletion
async function deleteFromS3(key) {
  const deleteParams = {
    Bucket: process.env.AWS_S3_BUCKET_NAME,
    Key: key,
  };
  try {
    await s3.deleteObject(deleteParams).promise();
    console.log(`Successfully deleted ${key} from S3`);
  } catch (error) {
    console.error(`Failed to delete ${key} from S3`, error);
  }
}

// AES encryption function using environment-specific salt and key
function encrypt(text) {
  const algorithm = 'aes-256-cbc';
  const salt = Buffer.from(process.env.AES_SALT, 'hex'); // Using the salt from environment
  const key = crypto.scryptSync(process.env.AES_SECRET_KEY, salt, 32);
  const iv = crypto.randomBytes(16); // Initialization vector
  const cipher = crypto.createCipheriv(algorithm, key, iv);

  let encrypted = cipher.update(text, 'utf8', 'hex');
  encrypted += cipher.final('hex');

  return `${iv.toString('hex')}:${encrypted}`; // Format: iv:encryptedData
}

// CSV UPLOAD - HELPER ROUTE
router.post('/upload', upload.single('file'), (req, res) => {
  if (!req.file) {
      return res.status(400).send('No file uploaded.');
  }
  res.send('File uploaded successfully.');
});


// POST STUDENT - Upload Form containing student information
router.post('/upload-student', authenticateToken, authorizeRoles('Admin', 'Staff'), async (req, res) => {
  const batchId = uuidv4(); // Unique batch ID for this upload using UUID
  let studentData = req.body;

  // Check if the necessary student data is present
  if (!studentData.studentId) {
    return res.status(400).send('Missing studentId');
  }

  try {
    let student = await Student.findOne({ studentId: studentData.studentId });

    if (!student) {
      // Create a new Student since one doesn't exist with the provided studentId
      student = new Student({
        ...studentData,
        batchIds: [batchId], // Initialize batchIds with the new batchId
      });
      await student.save();
    } else {
      // Update the existing student, add the new batchId if it's not already present
      if (!student.batchIds.includes(batchId)) {
        student.batchIds.push(batchId);
      }
      // Optionally, update any other student information here
      await student.save();
    }

    res.json({ batchId, student: student.toObject() });
  } catch (error) {
    console.error('Error processing student:', error);
    res.status(500).send('Error processing student');
  }
});

router.post('/upload-students', authenticateToken, authorizeRoles('Admin', 'Staff'), upload.single('file'), (req, res) => {
  if (!req.file) {
    return res.status(400).send('No file uploaded.');
  }

  const batchIds = [];
  const studentRows = [];
  let rollbackData = [];

  const stream = require('stream');
  const bufferStream = new stream.PassThrough();
  bufferStream.end(req.file.buffer);

  bufferStream
    .pipe(csvParser())
    .on('data', (row) => {
      // Map CSV row data to student schema fields according to the new CSV structure
      studentRows.push({
        studentId: row['Student ID'].toString(), // Ensure the student ID is treated as a string
        email: row['Username'],
        college: row['College'],
        department: row['Department'],
        major: row['Major'],
        firstName: row['First Name'],
        lastName: row['Last Name'],
        yearStanding: parseInt(row['Year Standing']), // Ensure that yearStanding is an integer
      });
    })
    .on('end', async () => {
      const batchId = uuidv4();
      try {
        const studentPromises = studentRows.map(async (row) => {
          let student = await Student.findOne({ studentId: row.studentId });
          if (!student) {
            student = new Student({ ...row, batchIds: [batchId] });
            await student.save();
            rollbackData.push({ action: 'create', id: student._id });
          } else {
            const oldBatchIds = [...student.batchIds];
            if (!student.batchIds.includes(batchId)) {
              student.batchIds.push(batchId);
            }
            await student.save();
            rollbackData.push({ action: 'update', id: student._id, oldBatchIds });
          }
          return student.toObject();
        });

        const studentsData = await Promise.all(studentPromises);
        batchIds.push(batchId);
        if (studentsData.length > 0) {
          res.json({ batchIds, studentsData });
        } else {
          throw new Error('No students were processed.');
        }
      } catch (error) {
        console.error('Error processing students:', error);
        // Rollback logic here
        for (let data of rollbackData) {
          if (data.action === 'create') {
            await Student.findByIdAndDelete(data.id);
          } else if (data.action === 'update') {
            await Student.findByIdAndUpdate(data.id, { batchIds: data.oldBatchIds });
          }
        }
        res.status(500).send('Error processing file');
      }
    })
    .on('error', (error) => {
      console.error('Stream error:', error);
      res.status(500).send('Error processing file');
    });
});


// router.post('/upload-students', authenticateToken, authorizeRoles('Admin', 'Staff'), upload.single('file'), (req, res) => {
//   if (!req.file) {
//     return res.status(400).send('No file uploaded.');
//   }

//   const batchIds = [];
//   const studentRows = [];
//   let rollbackData = [];

//   const stream = require('stream');
//   const bufferStream = new stream.PassThrough();
//   bufferStream.end(req.file.buffer);

//   bufferStream
//     .pipe(csvParser())
//     .on('data', (row) => {
//       studentRows.push(row);
//     })
//     .on('end', async () => {
//       const batchId = uuidv4();
//       try {
//         const studentPromises = studentRows.map(async (row) => {
//           let student = await Student.findOne({ studentId: row.studentId });
//           if (!student) {
//             student = new Student({ ...row, batchIds: [batchId] });
//             await student.save();
//             rollbackData.push({ action: 'create', id: student._id });
//           } else {
//             const oldBatchIds = [...student.batchIds];
//             if (!student.batchIds.includes(batchId)) {
//               student.batchIds.push(batchId);
//             }
//             await student.save();
//             rollbackData.push({ action: 'update', id: student._id, oldBatchIds });
//           }
//           return student.toObject();
//         });

//         const studentsData = await Promise.all(studentPromises);
//         batchIds.push(batchId);
//         if (studentsData.length > 0) {
//           res.json({ batchIds, studentsData });
//         } else {
//           throw new Error('No students were processed.');
//         }
//       } catch (error) {
//         console.error('Error processing students:', error);
//         // Rollback logic here
//         for (let data of rollbackData) {
//           if (data.action === 'create') {
//             await Student.findByIdAndDelete(data.id);
//           } else if (data.action === 'update') {
//             await Student.findByIdAndUpdate(data.id, { batchIds: data.oldBatchIds });
//           }
//         }
//         res.status(500).send('Error processing file');
//       }
//     })
//     .on('error', (error) => {
//       console.error('Stream error:', error);
//       res.status(500).send('Error processing file');
//     });
// });

router.post('/rollback-students', authenticateToken, authorizeRoles('Admin', 'Staff'), async (req, res) => {
  const { batchId } = req.body;
  if (!batchId) {
      return res.status(400).send('Batch ID is required for rollback.');
  }

  try {
      const affectedStudents = await Student.find({ batchIds: batchId });
      const rollbackPromises = affectedStudents.map(student => {
          student.batchIds = student.batchIds.filter(id => id !== batchId);
          return student.save();
      });

      await Promise.all(rollbackPromises);

      // After rolling back, perform cleanup
      const cleanupCount = await cleanupIncompleteCertificates();
      res.send({ message: 'Rollback and cleanup successful', batchId: batchId, cleaned: cleanupCount });
  } catch (error) {
      console.error('Rollback error:', error);
      res.status(500).send('Failed to rollback changes');
  }
});

// POST CERT ID - Initialize generated cert data with IDs
router.post('/assign-certificate-ids', authenticateToken, authorizeRoles('Admin', 'Staff'), async (req, res) => {
  const { batchId, createdBy } = req.body;
  if (!batchId) {
    console.log('No batch ID provided');
    return res.status(400).send('Batch ID is required.');
  }

  if (!createdBy) {
    console.log('No creator information provided');
    return res.status(400).send('Creator information is required.');
  }

  try {
    // Find students by the batchId stored in their batchIds array
    const students = await Student.find({ batchIds: batchId });

    if (!students.length) {
      console.log('No students found for batch ID:', batchId);
      return res.status(404).send('No students found for the provided batch ID.');
    }

    const updates = students.map(student => {
      const certificateId = `${student.studentId}-${uuidv4()}`;
      console.log(`Generating certificateId for student ${student.studentId}: ${certificateId}`);
      return generatedCertificates.updateOne(
        { studentInfo: student._id,
          batchProcessId: batchId },
        {
          $setOnInsert: {
            certificateId,
            certificateURL: '',
            selectedTemplate: '',
            createdBy: createdBy,
            batchProcessId: batchId
          },
          $set: {
            studentInfo: student._id,
          }
        },
        { upsert: true }
      );
    });

    await Promise.all(updates);

    // Retrieve updated certificate data
    const updatedStudents = await Student.find({ batchIds: batchId }, 'firstName lastName email');
    const certificates = await generatedCertificates.find({
      'studentInfo': { $in: updatedStudents.map(s => s._id) },
      'batchProcessId': batchId // This line ensures that you only get certificates for the current batch
    }).populate('studentInfo');

    res.json(certificates.map(cert => ({
      certificateId: cert.certificateId,
      firstName: cert.studentInfo.firstName,
      lastName: cert.studentInfo.lastName,
      email: cert.studentInfo.email
    })));

  } catch (error) {
    console.error('Error processing certificate IDs:', error);
    return res.status(500).send('Internal server error');
  }
});

// Route to cleanup incomplete certificates
router.post('/cleanup-incomplete-certificates', authenticateToken, authorizeRoles('Admin', 'Staff'), async (req, res) => {
  try {
      const count = await cleanupIncompleteCertificates(); // Call the cleanup function and get the count of cleaned certificates
      res.send({ message: `Cleanup process completed. Removed ${count} incomplete certificate(s).` });
  } catch (error) {
      console.error("Error during cleanup process:", error);
      res.status(500).send('Failed to clean up incomplete certificates');
  }
});

// POST - Batch Certificate Generation & Issuance
router.post('/generate-issue-certificates', authenticateToken, authorizeRoles('Admin', 'Staff'),  async(req, res, next)=> {

  const { batchId, templateId } = req.body;

  // Verify if both batchId and templateId are provided
  if (!batchId || !templateId) {
    res.status(400).send('Batch ID and Template ID are required');
    return;
  }

  // Fetch all students based on the batchId
  const students = await Student.find({ batchIds: batchId });

  if (!students.length) {
    res.status(404).send('No students found for the given batch ID');
    return;
  }

  // Fetch the template to ensure it exists
  const template = await certTemplate.findById(templateId);
  if (!template) {
    res.status(404).send('Template not found');
    return;
  }

  // Fetch the associated certificates for all students in the batch
  const certificates = await generatedCertificates.find({
    'studentInfo': { $in: students.map(s => s._id) },
    'batchProcessId': batchId
  }).populate('studentInfo');

  if (!certificates.length) {
    res.status(404).send('No certificates found for the given batch ID');
    return;
  }

  const results = await Promise.all(certificates.map(certificate =>
    generateAndIssueCertificate(certificate, template, batchId)
  ));

  const errors = results.filter(result => !result.success);
  if (errors.length > 0) {
    return res.status(500).send({
      message: 'Some errors occurred during certificate generation. Not all certificates were issued successfully.',
      errors: errors,
    });
  }
  console.log('Starting sending of certificates')
  // TEMPORARILY DISABLE EMAIL NOTIF FOR TESTING
  const filteredResults = results.filter(result => result.success).map((item)=>{
    const subject = `CMO EVENT | CERTIFICATE OF ATTENDANCE - ${item.certStudentName}`;
    const text = `Dear ${item.certStudentName},\n\nGreetings of peace!\n\nThank you for attending this year's event, listed herewith in this email is the PDF view link to your official certificate.\n\nYour certificate is available here: ${item.certificateURL}\n\nAMDG.`;

    return{
    to:item?.certificate?.studentInfo?.email,
    from: process.env.GMAIL_USER,
    subject,
    text
  }})

  sgMail.send(filteredResults)

  
  // await Promise.all(filteredResults.map((result, index) => {
  //   return new Promise((resolve) => {
  //     setTimeout(() => {
  //       sendCertificateEmail(result.certificate, result.certificateURL, result.certStudentName)
  //         .then(() => resolve()); // Resolve the promise after sending the email
  //     }, index * 1200); // Introduce a 1-second delay for each iteration
  //   });
  // }));
  // console.log('Finished sending, initiating cleanup')
  res.send({
    message: 'Batch certificate generation and emailing completed.',
    count: results.length - errors.length
  });
  await cleanupIncompleteCertificates();
});

// Utility function to generate and issue a certificate for a student
async function generateAndIssueCertificate(certificate, template, batchId) {

let s3Key = null;
let certificateCreated = false;
let certURL = null;

try {

  const { certBgImgKey, certEventYearLevel, certEventType, certEventTheme, certEventDate, certEventVenue, certDirectorName, certSigImgKey } = template;
  const { studentInfo, certificateId } = certificate;
  const { firstName, lastName, email, studentId } = studentInfo;
  const certStudentName = `${firstName} ${lastName}`;

  // Format the current date and time with milliseconds
  const createdAt = new Date();
  const formattedDate = createdAt.toISOString().replace(/[:-]/g, '').replace('T', '_').replace(/\..+/, '');
  const filename = `${studentId}-${formattedDate}.pdf`;

  const bgImgKey = extractKeyFromUrl(certBgImgKey);
  const sigImgKey = extractKeyFromUrl(certSigImgKey);

  // Create and populate the PDF document
  const doc = new PDFDocument({ layout: 'landscape', size: 'A4' });
  let buffers = [];
  doc.on('data', buffers.push.bind(buffers));

  // Font assets path
  const fontsPath = `${process.cwd()}/assets/certFonts/`;
  const imagePath = `${process.cwd()}/assets/Cert_XU-LOGO.png`;

  // Margin
  const distanceMargin = 18;

  // Calculate page dimensions for landscape orientation
  const pdfWidth = 841.89;
  const pdfHeight = 595.28;

  // Header
  const maxWidth = 250;
  const maxHeight = 250;

  // Fetch background image from S3
  const bgImageParams = {
    Bucket: process.env.AWS_S3_BUCKET_NAME,
    Key: bgImgKey,
  };
  console.log("Background image key:", bgImgKey);

  const bgImageResponse = await s3.getObject(bgImageParams).promise();
  const bgImageBuffer = bgImageResponse.Body;
  doc.image(bgImageBuffer, 0, 0, {
    width: doc.page.width,
    height: doc.page.height
  });

  doc.fillColor('black');

  // Adding semi-transparent background and image for the logo
  doc.rect(0, 0, doc.page.width, doc.page.height)
    .fillOpacity(0.8)
    .fill('white')
    .fillOpacity(1); // Reset fill opacity

  doc
  .fillAndStroke('#0e8cc3')
  .lineWidth(20)
  .lineJoin('round')
  .rect(
    distanceMargin,
    distanceMargin,
    doc.page.width - distanceMargin * 2,
    doc.page.height - distanceMargin * 2,
  )
  .stroke();
  
  // XU Logo Image
  doc.image(imagePath, doc.page.width / 2 - maxWidth / 2, 5, {
    fit: [maxWidth, maxHeight],
    align: 'center',
  });

  // Add "CERTIFICATE OF ATTENDANCE" text
  doc.font(`${fontsPath}LibreBaskerville-Bold.ttf`)
  .fontSize(20)
  .fill('#000000')
  .text('CERTIFICATE OF ATTENDANCE', 0, pdfHeight / 2 - 110, {
    align: 'center',
    width: pdfWidth,
  });

  // Add "is hereby given to" text
  doc.font(`${fontsPath}LibreBaskerville-Regular.ttf`)
  .fontSize(12)
  .fill('#000000')
  .text('is hereby given to', 0, pdfHeight / 2 - 80, {
    align: 'center',
    width: pdfWidth,
  });

  // Center the student's name
  doc.font(`${fontsPath}MonteCarlo-Regular.ttf`)
  .fontSize(40)
  .fill('#000000')
  .text(certStudentName, 0, pdfHeight / 2 - (50), {
     align: 'center',
    width: pdfWidth });

  // Underline student name
  const textWidth = doc.widthOfString(certStudentName);
  doc.moveTo((pdfWidth - textWidth) / 2, pdfHeight / 2 + 5) // Adjust Y-coordinate to position the line properly
  .lineTo((pdfWidth + textWidth) / 2, pdfHeight / 2 + 5) // Adjust Y-coordinate to position the line properly
  .lineWidth(1)
  .stroke('#000000');

  // Add body text
  doc.font(`${fontsPath}LibreBaskerville-Regular.ttf`)
  .fontSize(14)
  .fill('#000000')
  .text('for having attended the '+ certEventYearLevel +' Year Student\'s '+ certEventType +' \n of Xavier University - Ateneo De Cagayan with the theme: \n '+ certEventTheme +'\n on '+ certEventDate +'\n at '+ certEventVenue +'', 0, pdfHeight / 2 - (-50), {
    align: 'center',
    width: pdfWidth,
  });

  // Center CMO Director's name
  doc.font(`${fontsPath}LibreBaskerville-Regular.ttf`)
  .fontSize(14)
  .fill('#000000')
  .text(certDirectorName, 0, pdfHeight / 2 - (-150), {
    align: 'center',
    width: pdfWidth,
  });

  // Draw a black line under the text
  const directorTextWidth = doc.widthOfString(certDirectorName);
  doc.moveTo((pdfWidth - directorTextWidth) / 2, pdfHeight / 2 - (-160) + 5) // Adjust Y-coordinate to position the line properly
  .lineTo((pdfWidth + directorTextWidth) / 2, pdfHeight / 2 - (-160) + 5) // Adjust Y-coordinate to position the line properly
  .lineWidth(1)
  .stroke('#000000');

  // Center CMO Director subtext
  doc.font(`${fontsPath}LibreBaskerville-Regular.ttf`)
  .fontSize(12)
  .fill('#000000')
  .text('Director, College Campus Ministries', 0, pdfHeight / 2 - (-170), {
    align: 'center',
    width: pdfWidth,
  });

  // Fetch signature image from S3
  const sigImageParams = {
    Bucket: process.env.AWS_S3_BUCKET_NAME,
    Key: sigImgKey,
  };
  console.log("Signature image key:", sigImgKey);

  const sigImageResponse = await s3.getObject(sigImageParams).promise();
  const sigImageBuffer = sigImageResponse.Body;
  doc.image(sigImageBuffer, doc.page.width / 2 - maxWidth / 2, 400, {
        fit: [maxWidth, maxHeight],
        align: 'center',
      });

  // Generate QR Code and place it at the bottom right of the page
  const qrSize = 100;
  const bottomMargin = 30;

  const qrX = pdfWidth - qrSize - 30;
  const qrY = pdfHeight - qrSize - bottomMargin;

    // Encrypt certificateId
    const encryptedCertificateId = encrypt(certificateId.toString());
    const qrData = encryptedCertificateId;

    const qrCode = await QRCode.toDataURL(qrData);

  doc.image(qrCode, qrX, qrY, { width: qrSize, height: qrSize });
    
  doc.end();

  // Await buffer completion and handle PDF upload, update, and email
  await new Promise((resolve, reject) => {
    doc.on('end', () => resolve());
    doc.on('error', err => reject(err));
  });

  const pdfData = Buffer.concat(buffers);
  s3Key = `Generated/${filename}`;

  // Upload the PDF to S3 and update the database with the certificate URL
  const uploadParams = {
    Bucket: process.env.AWS_S3_BUCKET_NAME,
    Key: s3Key,
    Body: pdfData,
    ContentType: 'application/pdf'
  };


  const uploadResponse = await s3.upload(uploadParams).promise();
  certURL = uploadResponse.Location;

  // UNCOMMENT NEXT LINE TO TEST ROLLBACK - Place before upload or DB Update
  // throw new Error('Simulated system failure.');


    // Update database record
    await generatedCertificates.findByIdAndUpdate(
      { _id: certificate._id },
      {
        $set: {
          certificateURL: certURL,
          selectedTemplate: template._id,
          createdAt: createdAt,
        },
      }
    );

    certificateCreated = true;

    return {
      success: true,
      certificateId: certificate._id,
      certificate: certificate,
      certStudentName: certStudentName,
      s3Key: s3Key,
      certificateURL: certURL
    };
  } catch (error) {
    console.error("Error in certificate generation:", error);

    // Cleanup logic for certificate creation and S3 upload
    await cleanupAfterError(certificate, certificateCreated, s3Key, batchId);

    return {
      success: false,
      certificate: certificate,
      s3Key: s3Key,
      error: error
    };
  }
}

async function cleanupIncompleteCertificates() {
  try {
    // Identify and fetch all certificates that are incomplete.
    const incompleteCertificates = await generatedCertificates.find({
      certificateURL: { $eq: "" },
      selectedTemplate: { $eq: "" }
    }, 'studentInfo batchProcessId');  // Include batchProcessId in the projection.

    // Delete all identified incomplete certificates and update student records accordingly.
    for (let cert of incompleteCertificates) {
      // Delete the incomplete certificate.
      await generatedCertificates.deleteOne({ _id: cert._id });

      // Check if the studentInfo and batchProcessId exist before attempting to update.
      if (cert.studentInfo && cert.batchProcessId) {
        // Remove the batchProcessId from the student's batchIds array.
        await Student.findByIdAndUpdate(cert.studentInfo, {
          $pull: { batchIds: cert.batchProcessId }
        });
      }
    }

    console.log(`Cleanup process completed. Removed ${incompleteCertificates.length} incomplete certificate(s) and updated associated student records.`);
  } catch (error) {
    console.error("Error during cleanup of incomplete certificates and updating students:", error);
  }
}

// Define the cleanup logic in a separate function for clarity
async function cleanupAfterError(certificate, certificateCreated, s3Key, batchId) {
  // Assuming certificateCreated implies that certificate and related student update might need cleanup
  // and s3Key implies that an S3 object might need removal.

  if (certificateCreated || s3Key) {
    // Attempt to delete the certificate record if it was created or if there is any S3 key,
    // which suggests that we proceeded far enough that a cleanup is necessary.
    try {
      await generatedCertificates.findByIdAndDelete(certificate._id);
      console.log(`Removed certificate document for ${certificate._id}`);
    } catch (err) {
      console.error(`Error removing certificate document for ${certificate._id}: ${err}`);
    }

    // Attempt to update the student record if the certificate was created or any S3 operation occurred.
    try {
      await Student.findByIdAndUpdate(certificate.studentInfo._id, {
        $pull: { batchIds: batchId }
      });
      console.log(`Removed batchId: ${batchId} for ${certificate.studentInfo._id}`);
    } catch (err) {
      console.error(`Error updating student record ${certificate.studentInfo._id}: ${err}`);
    }
  }

  // Attempt to delete the S3 object if its key is known, regardless of the certificate creation state.
  if (s3Key) {
    try {
      await deleteFromS3(s3Key);
      console.log(`Removed S3 Certificate PDF with key - ${s3Key}`);
    } catch (err) {
      console.error(`Error removing S3 Certificate PDF with key ${s3Key}: ${err}`);
    }
  }
}


// Function to send an email via NODEMAILER
async function sendCertificateEmail(certificate, certURL, certStudentName) {

  // Email data with unicode symbols
  const studentEmail = certificate.studentInfo.email;
  const subject = `CMO EVENT | CERTIFICATE OF ATTENDANCE - ${certStudentName}`;
  const body = `Dear ${certStudentName},\n\nGreetings of peace!\n\nThank you for attending this year's event, listed herewith in this email is the PDF view link to your official certificate.\n\nYour certificate is available here: ${certURL}\n\nAMDG.`;

  // Send mail with defined transport object
  try {
    console.log('Starting sendCertificateEmail trigger')
    let info = await transporter.sendMail({
      from: `${process.env.GMAIL_USER}`, // sender address
      to: studentEmail, // list of receivers
      subject: subject, // Subject line
      text: body, // plain text body
    });
    console.log('Finished sendCertificateEmail trigger')
    console.log('Message sent: %s', info.messageId);
  } catch (error) {
    console.error(`Failed to send email to ${studentEmail}`, error);
  }
}

  
module.exports = router;