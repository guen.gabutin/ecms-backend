const express = require('express');
const router = express.Router();

const student = require('../models/studentsSchema')
const generatedCertificates = require('../models/certificatesSchema')
const cmoEvent = require('../models/eventsSchema')

const { authenticateToken, authorizeRoles } = require('../middleware');

const getEventsNextWeek = async () => {
    const today = new Date();
    const nextWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7);

    // Set the time for today to 00:00:00 to include all events from today onwards
    today.setHours(0, 0, 0, 0);
    // Set the time for nextWeek to 23:59:59 to include all events up to the end of the day
    nextWeek.setHours(23, 59, 59, 999);

    // Query to find events where eventDate is within the next week
    const query = {
        eventDate: {
            $gte: today,
            $lte: nextWeek
        }
    };

    return await cmoEvent.countDocuments(query);
}

const getEventsCurrentMonth = async () => {
    const currentDate = new Date();
    const firstDayOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
    const lastDayOfMonth = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);

    // Set the time for the first and last day of the month to include all events
    firstDayOfMonth.setHours(0, 0, 0, 0);
    lastDayOfMonth.setHours(23, 59, 59, 999);

    // Query to find events where eventDate is within the current month
    const query = {
        eventDate: {
            $gte: firstDayOfMonth,
            $lte: lastDayOfMonth
        }
    };

    return await cmoEvent.countDocuments(query);
}


// GET Route to count all documents in the student collection
router.get('/card-totals', authenticateToken, authorizeRoles('Admin', 'Staff', 'StudentAssistant'), async (req, res) => {
    try {
        // Count all documents within the generatedCertificates collection
        const totalStudents = await student.countDocuments();
        const totalCertificates = await generatedCertificates.countDocuments();
        const eventsNextWeek = await getEventsNextWeek();
        const eventsThisMonth= await getEventsCurrentMonth();

        // Return the total count as JSON
        res.json({ totalStudents, totalCertificates, eventsNextWeek, eventsThisMonth });
    } catch (error) {
        // Handle any errors that occur during the process
        console.error('Error counting students:', error);
        res.status(500).send('Internal server error');
    }
});


module.exports = router;