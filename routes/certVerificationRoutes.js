require('dotenv').config();

const express = require('express');
const router = express.Router();

const crypto = require('crypto');

const generatedCertificates = require('../models/certificatesSchema')

const { authenticateToken, authorizeRoles } = require('../middleware');
  
  // AES decryption function compatible with the above encryption logic
  function decrypt(encryptedData) {
    const parts = encryptedData.split(':');
    if (parts.length !== 2) {
      throw new Error("Invalid encrypted data format");
    }
  
    const salt = Buffer.from(process.env.AES_SALT, 'hex'); // Using the salt from environment
    const iv = Buffer.from(parts[0], 'hex');
    const encryptedText = parts[1];
    const key = crypto.scryptSync(process.env.AES_SECRET_KEY, salt, 32);
    const decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
  
    let decrypted = decipher.update(encryptedText, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
  }
  
  // GET Route to verify AES-Encrypted QR Code from Generated Certificate
  // * Please use 'Html5QrcodeScanner' from 'html5-qrcode' for Frontend
  router.get('/scan-certificate', authenticateToken, authorizeRoles('Admin', 'Staff', 'StudentAssistant'), async (req, res) => {
    const { encryptedData } = req.query;
  
    if (!encryptedData) {
      return res.status(400).send({ error: 'Encrypted data is required.' });
    }
  
    try {
      // Decrypt the certificate ID from the encrypted data
      const secretKey = process.env.AES_SECRET_KEY; // Make sure you've set this in your environment
      const decryptedCertificateId = decrypt(encryptedData, secretKey);
  
      // Find the certificate using the decrypted certificate ID
      const certificate = await generatedCertificates.findOne({ certificateId: decryptedCertificateId }).populate('studentInfo');
  
      if (!certificate) {
        return res.status(404).send({ valid: false });
      }
  
      // Return the certificate information
      res.json({
        valid: true,
        data: {
          Name: `${certificate.studentInfo.firstName} ${certificate.studentInfo.lastName}`,
          CertificateId: certificate.certificateId,
          CertificateURL: certificate.certificateURL,
          DateGenerated: certificate.createdAt,
          CreatedBy: certificate.createdBy
        }
      });
    } catch (error) {
      console.error('Error verifying certificate:', error);
      res.status(500).send({ valid: false, message: 'Internal server error' });
    }
  });


module.exports = router;