require('dotenv').config();
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const bodyParser = require ('body-parser')
const bcrypt = require ('bcryptjs')

const port = process.env.PORT
const dbURI = process.env.DB_URI

const User = require('./models/userSchema')

const userRoutes = require('./routes/userRoutes')
const dashboardCardRoutes = require('./routes/dashboardCardRoutes')
const eventRoutes = require('./routes/eventRoutes')
const studentRoutes = require('./routes/studentRoutes')
const certTemplateRoutes = require('./routes/certTemplateRoutes')
const certGenerationRoutes = require('./routes/certGenerationRoutes')
const certVerificationRoutes = require('./routes/certVerificationRoutes')


// Connect to express app
const app = express();

// Connect to MongoDB
mongoose.connect(dbURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then (() => {
    app.listen(port, () => {
        console.log('Server is connected to port 3001 and connected to MongoDB')
        createInitialAdmin();
    })
  })
  .catch ((error) => {
    console.log('Unable to connect to Server and/or MongoDB')
  })

  // CORS options
  // const corsOptions = {
  //   origin: 'https://e-cms-capstone.vercel.app', // Allow only your frontend domain
  //   optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  // };

  async function createInitialAdmin() {
    const exists = await User.findOne({ role: 'Admin' });
    if (!exists) {
      let adminPassword = process.env.ADMIN_INITIAL_PASSWORD;
      let adminEmail = process.env.ADMIN_INITIAL_EMAIL;
      let adminUsername = process.env.ADMIN_INITIAL_USERNAME;
  
      // Ideally, add validation for these environment variables.
      const hashedPassword = await bcrypt.hash(adminPassword, 10);
      const adminUser = new User({
        email: adminEmail,
        username: adminUsername,
        password: hashedPassword,
        role: 'Admin'
      });
  
      await adminUser.save();
      console.log('Initial admin account created');
    }
  }


// Middleware
app.use(bodyParser.json())
app.use(cors()); // Use CORS middleware with options
app.use(express.urlencoded({ extended: true }));

// Routes
app.use('/users', userRoutes)
app.use('/dashboard-cards', dashboardCardRoutes)
app.use('/cmo-events', eventRoutes)
app.use('/students', studentRoutes)
app.use('/certificate-templates', certTemplateRoutes)
app.use('/generated-certificates', certGenerationRoutes)
app.use('/verify-certificates', certVerificationRoutes)
