const mongoose = require ('mongoose')
const certTemplate = require ('./certTemplateSchema')
const student = require ('./studentsSchema')

const certCSVSchema = new mongoose.Schema({
    certificateId: { type: String, required: true, unique: true },
    certificateURL: { type: String, default: '' },
    selectedTemplate: { type: String, default: '' },
    studentInfo: { 
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'students' },
    batchProcessId: { type: String, required: true },
    createdBy: { type: String, type: String, default: '' },
    createdAt: { type: Date, default: Date.now }

})

certCSVSchema.pre('save', function(next){
    this.createdAt = new Date();
    next();
})

const generateCertificate = mongoose.model('generatedCertificates', certCSVSchema)

module.exports = generateCertificate 