const mongoose = require ('mongoose')

const eventSchema = new mongoose.Schema({
    eventDate: { type: Date },
    department: { type: String },
    description: { type: String },
    batch: { type: String },
    yearLevel: { type: String },
    venue: { type: String },
    inCharge: { type: String }

})

const cmoEvent = mongoose.model('cmoEvents', eventSchema)

module.exports = cmoEvent