const mongoose = require ('mongoose')

const certTemplateSchema = new mongoose.Schema({
    templateTitle: { type: String },
    certBgImgKey: { type: String },
    certStudentName: {type: String },
    certEventYearLevel: { type: String },
    certEventType: { type: String },
    certEventTheme: { type: String },
    certEventDate: { type: String },
    certEventVenue: { type: String },
    certDirectorName: { type: String },
    certSigImgKey: { type: String }
});

const certTemplate = mongoose.model('certTemplates', certTemplateSchema)

module.exports = certTemplate