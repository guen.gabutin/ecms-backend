const mongoose = require ('mongoose')

const userSchema = new mongoose.Schema({
    email: {type: String, required: true},
    username: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    role: {type: String, enum: ['Admin', 'Staff', 'StudentAssistant'], default: 'Staff'},
    resetPasswordToken: { type: String },
    resetPasswordExpires: { type: Date }   
})

const User = mongoose.model('User', userSchema)

module.exports = User