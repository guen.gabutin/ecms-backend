const mongoose = require ('mongoose')

const studentSchema = new mongoose.Schema({
   studentId: { type: String, required: true },
   college: { type: String },
   department: { type: String },
   // program: { type: String},
   major: {type: String },
   email: { type: String },
   firstName: { type: String },
   lastName: { type: String },
   yearStanding: { type: String },
   batchIds: { type: [String] }

})

const student = mongoose.model('students', studentSchema)

module.exports = student