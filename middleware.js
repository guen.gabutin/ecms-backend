// middleware.js
const jwt = require('jsonwebtoken');

// SECRET_KEY should be set in Vercel's environment variables configuration
const SECRET_KEY = process.env.SECRET_KEY;

const authenticateToken = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1]; // Bearer TOKEN

  if (token == null) {
      return res.status(401).json({ error: 'No token provided' });
  }

  jwt.verify(token, SECRET_KEY, (err, user) => {
      if (err) {
          // Check if the error is due to an expired token
          if (err.name === 'TokenExpiredError') {
              return res.status(401).json({ error: 'Token expired' });
          } else {
              return res.status(403).json({ error: 'Invalid token' });
          }
      }
      req.user = user;
      next();
  });
};

  const authorizeRoles = (...permittedRoles) => {
    // Return middleware that checks for the user's role
    return (req, res, next) => {
      const { user } = req;
      if (user && permittedRoles.includes(user.role)) {
        next(); // role is allowed, so continue on the next middleware
      } else {
        res.status(403).json({message: "Forbidden"}); // user is forbidden
      }
    };
  };

  module.exports = {
    authenticateToken,
    authorizeRoles
};